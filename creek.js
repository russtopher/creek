(function() {
    function assert(condition) {
        if (!condition) {
            throw "assertion failed";
        }
    }

    assert(true);
    assert(1==1);
    assert(!(1==2));

    var TileType = {};
    TileType.Grass = "grass";
    TileType.Blank = "blank";
    TileType.Creek = "Creek";

    var Game = function() {
        this.width = 6;
        this.height = 10;
        this.board = [];
        for (var row = 0; row < this.height; row++) {
            this.board[row] = [];
            for (var col = 0; col < this.width; col++) {
                this.board[row][col] = TileType.Blank;
            }
        }
    };

    var renderer = new PIXI.WebGLRenderer(500, 400);

    document.body.appendChild(renderer.view);

    var grassTexture = PIXI.Texture.fromImage("grass.png");
    var creekTexture = PIXI.Texture.fromImage("creek.png");
    var blankTexture = PIXI.Texture.fromImage("blank.png");

    var buildStage = function(game) {
        var stage = new PIXI.Stage(0x66FF99);
        stage.interactive = true;
        for (var row = 0; row < game.height; row++) {
            for (var col = 0; col < game.width; col++) {
                switch (game.board[row][col]) {
                    case TileType.Blank:
                        var s = new PIXI.Sprite(blankTexture);
                        break;
                    default:
                        assert(false);
                }
                s.row = row;
                s.col = col;
                s.position.x = 100 + col * 32;
                s.position.y = 50 + row * 32;
                s.interactive = true;
                s.mousedown = function(data) {
                    console.log("%o", data);
                };
                stage.addChild(s);
            }
        }
        return stage;
    };

    var game = new Game();

    function draw() {
        var stage = buildStage(game);
        renderer.render(stage);
        window.requestAnimationFrame(draw);
    }

    window.requestAnimationFrame(draw);
})();
